<h1>SE_BRAVO_520 🎃</h1>
![npm](https://img.shields.io/npm/v/npm)
![npm](https://img.shields.io/npm/v/react?label=react)
![npm](https://img.shields.io/npm/v/next?label=nextjs)
![npm](https://img.shields.io/npm/v/express?label=express)
![npm](https://img.shields.io/npm/v/eslint?label=eslint)

<h2>READ ME BEFORE START</h2>

**CLIENT AND SERVER HOST** <br/>
| HOST | LINK |
| ------ | ------ |
| CLIENT | `https://bravoclient.herokuapp.com/` |
| SERVER | `https://sebravoserver.herokuapp.com/` |

**BACKEND API** <br/>
| APP | LINK |
| ------ | ------ |
| POSTMAN | `https://app.getpostman.com/join-team?invite_code=d65d729a006222c7d3ede7e3f266387d&ws=3f2c7441-6216-454d-923a-002296f2b21c` |


**Run project** <br/>
| os | command |
| ------ | ------ |
| windows | `.\script\windows\run.bat` |
| linux | `.\script\linux\run.sh` |

**Stop project** <br/>
| os | command |
| ------ | ------ |
| windows | `.\script\windows\stop.bat` |
| linux | `.\script\linux\stop.sh` |

**Remove project** <br/>
| os | command |
| ------ | ------ |
| windows | `.\script\windows\remove.bat` |
| linux | `.\script\linux\remove.sh` |

**Docker unit test** <br/>
| os | command |
| ------ | ------ |
| windows | `.\script\windows\test\docker.bat` |
| linux | `.\script\linux\test\docker.sh` |

**Local unit test** <br/>
| os | command |
| ------ | ------ |
| windows | `.\script\windows\test\local.bat` |
| linux | `.\script\linux\test\local.sh` |
