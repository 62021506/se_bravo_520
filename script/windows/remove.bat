@echo off

echo "remove all container and image"
docker stop client
docker rm client 
docker rmi client

docker stop server
docker rm server 
docker rmi server