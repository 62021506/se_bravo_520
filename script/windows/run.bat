@echo off

echo "Running Script"
echo "Docker Build Server "
docker build server/. -t server

echo "Docker Build Client"
docker build client/. -t client
echo "Docker Run Client [port : 3000]"
docker run -d -p 3000:3000 --name client client

echo "Docker Run Server [port : 3001]"
docker run -d -p 3001:3001 --name server server