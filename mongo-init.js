db = db.getSiblingDB("admin");
db.createUser({
  user: "root1",
  pwd: "root",
  roles: [
    { role: "userAdminAnyDatabase", db: "admin" },
    { role: "dbAdminAnyDatabase", db: "admin" },
    { role: "readWriteAnyDatabase", db: "admin" },
  ],
});
db = db.getSiblingDB("bravo");
db.createUser({
  user: "romeo",
  pwd: "r1234",
  roles: [
    {
      role: "readWrite",
      db: "bravo",
    },
  ],
});

db = db.getSiblingDB("bravo");

db.createCollection("users");

db.users.insertMany([
  {
    username: "romeo56",
    password: "$2b$10$dcTAEtR8NtYQa2PZLrgeM.IxIZpJSkxpDL7DuRL/l.1UTyF5Ne.rK",
  },
  {
    username: "romeo",
    password: "$2b$10$dcTAEtR8NtYQa2PZLrgeM.IxIZpJSkxpDL7DuRL/l.1UTyF5Ne.rK",
  },
  {
    username: "romeo55",
    password: "$2b$10$dcTAEtR8NtYQa2PZLrgeM.IxIZpJSkxpDL7DuRL/l.1UTyF5Ne.rK",
  },
]);
