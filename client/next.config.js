module.exports = {
  poweredByHeader: false,
  reactStrictMode: true,
  webpackDevMiddleware: (config) => {
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
    };
    return config;
  },
  images: {
    domains: ["firebasestorage.googleapis.com","images.pexels.com"],
  },
};
