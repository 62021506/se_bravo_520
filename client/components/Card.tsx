import Image from "next/image";
import Link from "next/link";

const Card = (props: any) => {
  return (
    <>
      <Link href={`/product/${props.id}`}>
        <a className="group relative overflow-hidden">
          <Image
            className="bg-purple-300 h-full"
            width={500}
            height={300}
            layout="intrinsic"
            quality={100}
            objectFit="cover"
            src={props.image}
          />
          <h4 className="text-2xl">{props.name}</h4>
          <h4 className="text-gray-600">230 reviews</h4>
        </a>
      </Link>
    </>
  );
};

export default Card;
