import { storage } from "./config"
import { getDownloadURL, ref, uploadBytes } from "@firebase/storage"

export const onUploadToBlob = (
	file
) => {
	return new Promise((resolve) => {
		const reader = new FileReader()
		reader.addEventListener("load", (r) => {
			resolve(r.target?.result)
		})
		reader.readAsDataURL(file)
	})
}

export const onUploadToStorage = async (file , dir = "images") => {
	const storageRef = ref(storage, `${dir}/${Date.now()}-${file.name}`)
	await uploadBytes(storageRef, file, {
		contentType: file.type,
	})

	return await getDownloadURL(storageRef)
}
