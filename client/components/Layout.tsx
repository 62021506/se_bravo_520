import Nav from "./Nav";
import Header from "./Header";
import styles from "../styles/newStyles/Layout.module.css";

const Layout = (props: any) => {
  return (
    <>
      <header>
        <Nav />
      </header>
      <main className="max-w-screen-xl mx-auto mt-3 px-2 xs:px-0">{props.children}</main>
    </>
  );
};

export default Layout;
