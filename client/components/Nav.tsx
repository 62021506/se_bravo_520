import Link from "next/link";
import navStyles from "../styles/newStyles/Nav.module.css";
import Auth from "../components/NavbarAuth";
import { isAuth } from "../lib/auth";
const Nav = () => {
  const auth: any = isAuth(
    (typeof window !== "undefined" && localStorage.getItem("jwt")) || ""
  );
  return (
    <nav className="bg-white py-2">
      <div className="flex max-w-screen-xl mx-auto px-2 xs:px-0">
        <div className="flex-1 sm:flex-none w-2/12">
          <div className={"logo text-3xl font-romeo2"}>
            <Link href="/">
              <a>
                <span className="text-purple-600">W</span>REVIEWS
              </a>
            </Link>
          </div>
        </div>
        <div className="flex-grow sm:block hidden">
          <form className="relative" method="GET">
            <svg
              width="20"
              height="20"
              fill="currentColor"
              className="absolute left-3 top-1/2 transform -translate-y-1/2 text-gray-400"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
              />
            </svg>
            <input
              className="focus:border-black focus:ring-2 focus:ring-black focus:outline-none w-full text-sm text-black placeholder-gray-500 border border-gray-200 py-2 pl-10"
              name="name"
              id="name"
              type="text"
              aria-label="Search Reviews"
              placeholder="Search Reviews"
            />
          </form>
        </div>
        {typeof window !== "undefined" && localStorage.getItem("jwt") ? (
          <Auth auth={auth}></Auth>
        ) : (
          <div className={`flex-1 sm:flex-none w-3/12" `}>
            <div className="flex justify-end pl-2 h-full">
              <Link href={`/login`}>
                <a className="flex justify-center bg-purple-600 text-white sm:bg-white sm:text-black items-center flex-grow h-full ml-1 text-purple focus:outline-none">
                  Sign In
                </a>
              </Link>
              <Link href={`/register`}>
                <div className="hidden sm:inline-block cursor-pointer">
                  <a className="flex justify-center items-center bg-purple-600 hover:black focus:outline-none focus:ring-2 focus:ring-purple-700 focus:ring-opacity-50 flex-grow h-full ml-2 text-white px-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                    <span className="ml-1"> Create Account</span>
                  </a>
                </div>
              </Link>
            </div>
          </div>
        )}
      </div>
    </nav>
  );
};

export default Nav;
