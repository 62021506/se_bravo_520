import headerStyles from '../styles/newStyles/Header.module.css'

const Header = () => {
    return (
        <div>
            <h1 className={headerStyles.title}>
                <span>WebDev</span> News
            </h1>
            <p className={headerStyles.description}>Keep up to date with the latset web dev news</p>
        </div> 
    )
}

export default Header
