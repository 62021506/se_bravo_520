// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

//
import { getStorage } from "firebase/storage"


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBjNeMHGoYCxL7jRY8WDO9p7K3ALCHs9TE",
  authDomain: "noname-71987.firebaseapp.com",
  projectId: "noname-71987",
  storageBucket: "noname-71987.appspot.com",
  messagingSenderId: "244973806725",
  appId: "1:244973806725:web:75103b5a8a4ccb1ad475ec",
  measurementId: "G-83XYPS623V"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

//
export const storage = getStorage(app)