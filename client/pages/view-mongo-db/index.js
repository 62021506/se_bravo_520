import Head from "next/head";

import bodyStyle from "../test-api/bodyStyle.module.css";

import headerStyles from "../../styles/newStyles/Header.module.css";

import axios from "axios";

const defaultEndpoint = `${process.env.NEXT_PUBLIC_SERVER}/product`;

import Image from "next/image";

export default function Home({ data }) {
  const { results = [] } = data;
  console.log("data", data);

  return (
    <div className="container">
      <Head>
        <title>api App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <body>
        <h1 className={headerStyles.title}>
          <span>ProductAPI</span> News
        </h1>
        <p className={headerStyles.description}>
          Keep up to date with the latset web dev news
        </p>

        <main>
          <ul className={bodyStyle.grid}>
            {results.map((result) => {
              const { _id, name, image, type, detail } = result;

              return (
                <li key={_id} className={bodyStyle.card}>
                  <a href={`/product/${_id}`}>
                    <Image
                      width={500}
                      height={400}
                      src={image}
                      alt={`${name}`}
                    ></Image>
                    <br />
                    <h3>{name}</h3>
                    <h3>{type}</h3>
                    <h3>{detail}</h3>
                  </a>
                </li>
              );
            })}
          </ul>
        </main>
      </body>
    </div>
  );
}

export async function getServerSideProps() {
  const res = await fetch(defaultEndpoint);
  console.log(res);
  const data = await res.json();
  return {
    props: {
      data,
    },
  };
}
