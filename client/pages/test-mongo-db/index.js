import Head from 'next/head'

import ArticleList from '../../components/article/ArticleList'

import headerStyles from '../../styles/newStyles/Header.module.css'
import { useState } from 'react'

import axios from 'axios'

// import upload from './upload'



import { storage } from "../../components/config"
import { getDownloadURL, ref, uploadBytes } from "@firebase/storage"

const onUploadToBlob = (
	file
) => {
	return new Promise((resolve) => {
		const reader = new FileReader()
		reader.addEventListener("load", (r) => {
			resolve(r.target?.result)
		})
		reader.readAsDataURL(file)
	})
}

const onUploadToStorage = async (file , dir = "images") => {
	const storageRef = ref(storage, `${dir}/${Date.now()}-${file.name}`)
	await uploadBytes(storageRef, file, {
		contentType: file.type,
	})

	return await getDownloadURL(storageRef)
}




export default function Home({ articles }) {

    const test = async (e) => {
        e.preventDefault()
        //new
        // console.log(upload)
        const url = await onUploadToStorage(image[0])

        axios.post(`${process.env.NEXT_PUBLIC_SERVER}/product/add`,{
            image:url , type , name , detail , review
        }, {
            headers:{
                authorization: `Berere ${localStorage.getItem("jwt")}`
            }
        })
    
    } 

    const [image, setImage] = useState()

    const [type, setType] = useState()

    const [name, setName] = useState()

    const [detail, setDetail] = useState()

    const [review, setReview] = useState()

    return (
        <div>

            <body>
                
                <form onSubmit={test} >
                    image : <input type="file" onChange={e => setImage(e.target.files) } /> 
                    <br/> 
                    <br/>

                    type : <input name="password" value={type} onChange={e => setType(e.target.value) } />
                    <br/>
                    <br/>

                    name : <input name="conPassword" value={name} onChange={e => setName(e.target.value) } />
                    <br/>
                    <br/>

                    detail : <input name="conPassword" value={detail} onChange={e => setDetail(e.target.value) } />
                    <br/>
                    <br/>

                    review : <input name="conPassword" value={review} onChange={e => setReview(e.target.value) } />
                    <br/>
                    <br/>

                    <button type="submit">send</button>
                </form>

            </body>

        </div>
    )
}

