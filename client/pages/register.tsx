import Head from "next/head";

import ArticleList from "../components/article/ArticleList";

import headerStyles from "../../styles/newStyles/Header.module.css";
import { useState } from "react";

import axios from "axios";
import Link from "next/link";
import Input from "../components/Input";
import { useRouter } from "next/router";

export default function Home(props: any) {
  const [errors, setErrors]: any = useState({});
  const [loading, setLoading]: any = useState(false);

  const router = useRouter();

  const onHanndleChangeValidate = (e: any) => {
    const { name } = e.target;
    setErrors({
      ...errors,
      [name]: "",
    });
  };

  const inputValidate = (e: any) => {
    let formIsValid = true;
    let errorser: any = {};
    if (e.target.name.value.length <= 0) {
      formIsValid = false;
      errorser["name"] = "Name is required";
    }
    if (e.target.username.value.length <= 0) {
      formIsValid = false;
      errorser["username"] = "Username is invalid";
    }
    if (e.target.email.value.length <= 0) {
      formIsValid = false;
      errorser["email"] = "Email is required";
    } else {
      const re =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (re.test(String(e.target.email.value).toLowerCase()) == false) {
        formIsValid = false;
        errorser["email"] = "Email is invalid";
      }
    }
    if (e.target.password.value.length <= 0) {
      formIsValid = false;
      errorser["password"] = "Password is required";
    }
    if (!e.target.checkbox.checked) {
      formIsValid = false;
      errorser["checkbox"] =
        "You must read and agree to the Terms of Use to continue";
    }
    setErrors(errorser);
    return formIsValid;
  };
  const register = async (e: any) => {
    e.preventDefault();
    if (!inputValidate(e)) return;
    const body = {
      username: e.target.username.value,
      password: e.target.password.value,
      email: e.target.email.value,
      name: e.target.name.value,
    };

    setLoading(true);
    const data: any = await axios.post(`${process.env.NEXT_PUBLIC_SERVER}/auth/register`, body);
    if (data.data.status == "Username already exists") {
      const errorser: any = {};
      setLoading(false);
      errorser["username"] = "Username already exists";
      setErrors(errorser);
      return;
    }
    if (data.data.status == true) {
      router.push("/login");
    }
    console.log(data);
  };

  return (
    <div>
      <div className="bg-white sm:p-11 p-5 shadow-md m-auto w-4/6">
        <h1 className="text-3xl font-romeo2">Create an Account</h1>
        <span className="text-lg mt-1 block">Join the community.</span>
        <br />
        <form onSubmit={register}>
          <Input
            name="name"
            label="* Name"
            placeholder="E.g. Ethan Winters"
            onChange={onHanndleChangeValidate}
            error={errors["name"]}
          />
          <Input
            name="username"
            label="* Username"
            placeholder="E.g. ethanWinters"
            error={errors["username"]}
            onChange={onHanndleChangeValidate}
          />
          <Input
            name="email"
            label="* Email"
            placeholder="E.g. ethan@blueum.com"
            error={errors["email"]}
            onChange={onHanndleChangeValidate}
          />
          <Input
            name="password"
            label="* Password"
            placeholder=""
            password={true}
            error={errors["password"]}
            onChange={onHanndleChangeValidate}
          />
          <div className="mb-2 block">
            <input
              name="checkbox"
              className="form-checkbox border-purple-700 text-purple-500 focus:outline-none focus:ring-2 focus:ring-purple-600"
              id="checkbox"
              type="checkbox"
              onChange={onHanndleChangeValidate}
            />
            <label htmlFor="checkbox" className="ml-2 select-none">
              By creating an account, you agree to our{" "}
              <Link href="/terms">
                <a className="text-purple-600">Terms of Use</a>
              </Link>{" "}
              and .
              <Link href="/privacy">
                <a className="text-purple-600">Privacy Policy</a>
              </Link>
              {errors["checkbox"] && (
                <div className="text-sm text-red-500">{errors["checkbox"]}</div>
              )}
            </label>
          </div>
          <button
            className="flex bg-purple-500 hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-purple-600 text-white py-2 px-5 cursor-pointer"
            type="submit"
            disabled={loading}
          >
            {loading && (
              <svg
                className="animate-spin -ml-1 mr-3 h-5 w-5 text-white"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <circle
                  className="opacity-25"
                  cx="12"
                  cy="12"
                  r="10"
                  stroke="currentColor"
                  strokeWidth="4"
                ></circle>
                <path
                  className="opacity-75"
                  fill="currentColor"
                  d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                ></path>
              </svg>
            )}
            <span>Create account</span>
          </button>
        </form>
        <br />
        <span>
          Already have an account?{" "}
          <Link href="/login">
            <a className="text-purple-600">Sign in</a>
          </Link>
        </span>
      </div>
      <br />
      <br />
    </div>
  );
}
