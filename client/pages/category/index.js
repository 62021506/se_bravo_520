import Head from 'next/head'

import headerStyles from '../../styles/newStyles/Header.module.css'

export default function Home({ data }) {
 
    return (
        <div className="container"> 
            <Head>
                <title>category</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <body>
                <h1 className={headerStyles.title}>
                    <span>All Product</span> category
                </h1>
                <p className={headerStyles.description}>have everything this you want</p>

            </body>

        </div>
    )
}
