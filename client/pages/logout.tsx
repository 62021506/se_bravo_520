import { useRouter } from "next/router";
import { useEffect } from "react";

const Logout = () => {
  const router = useRouter();
  useEffect(() => {
    if (typeof window !== "undefined" && localStorage.getItem("jwt")) {
      localStorage.removeItem("jwt");
      router.push("/", undefined, { shallow: true });
    }
  }, []);
  return <div className=""></div>;
};

export default Logout;
