// import 'tailwindcss/tailwind.css'
import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Layout from '../components/Layout'

import Head from 'next/head'

function MyApp({ Component, pageProps }: AppProps) {
  //new add Layout
  return (
    <Layout>
      
      <Head>
        <title>Reviews</title>
        <meta name='keyword' content='reviews' />
      </Head>

      <Component {...pageProps} />
    </Layout>
  )
}
export default MyApp
