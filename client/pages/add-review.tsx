import Head from "next/head";

import ArticleList from "../components/article/ArticleList";

import headerStyles from "../styles/newStyles/Header.module.css";

import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import axios from "axios";
import Input from "../components/Input";
import { storage } from "../components/config";
import { getDownloadURL, ref, uploadBytes } from "@firebase/storage";
import { useRouter } from "next/router";

const onUploadToStorage = async (file: any, dir = "images") => {
  const storageRef = ref(storage, `${dir}/${Date.now()}-${file.name}`);
  await uploadBytes(storageRef, file, {
    contentType: file.type,
  });

  return await getDownloadURL(storageRef);
};

export default function Home() {
  const [errors, setErrors]: any = useState({});
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const onHanndleChangeValidate = (e: any) => {
    const { name } = e.target;
    setErrors({
      ...errors,
      [name]: "",
    });
  };

  const inputValidate = (e: any) => {
    let formIsValid = true;
    let errorser: any = {};
    if (e.target.type.value.length <= 0) {
      formIsValid = false;
      errorser["type"] = "Type is required";
    }
    if (e.target.review.value.length <= 0) {
      formIsValid = false;
      errorser["review"] = "review is required";
    }
    if (e.target.name.value.length <= 0) {
      formIsValid = false;
      errorser["name"] = "Name is required";
    }
    if (e.target.image.files.length <= 0) {
      formIsValid = false;
      errorser["image"] = "Image is required";
    }

    setErrors(errorser);
    return formIsValid;
  };

  const postData = async (e: any) => {
    e.preventDefault();
    if (!inputValidate(e)) return;
    const url = await onUploadToStorage(e.target.image.files[0]);
    const body = {
      type: e.target.type.value,
      name: e.target.name.value,
      detail: e.target.detail.value,
      review: e.target.review.value,
      image: url,
    };
    axios
      .post(`${process.env.NEXT_PUBLIC_SERVER}/product/add`, body, {
        headers: {
          authorization: `Berere ${localStorage.getItem("jwt")}`,
        },
      })
      .then((res) => {
        console.log(res);
        router.push("/");
      });
    // console.log(body);
  };

  return (
    <>
      <div className="bg-white sm:p-11 p-5 shadow-md m-auto w-4/6">
        <h1 className="text-3xl font-romeo2">ADD REVIEWS</h1>
        <span className="text-lg mt-1 block">
          Welcome back to the community
        </span>
        <br />

        <form onSubmit={postData}>
          <Input
            name="type"
            label="Type : "
            placeholder="type"
            error={errors["type"]}
            onChange={onHanndleChangeValidate}
          />
          <Input
            name="name"
            label="Name : "
            placeholder="name"
            error={errors["name"]}
            onChange={onHanndleChangeValidate}
          />
          <textarea
            className="focus:ring-1  focus:outline-none w-full text-black placeholder-gray-500 border p-3"
            name="detail"
            id=""
            cols={30}
            rows={10}
            placeholder="detail"
          ></textarea>
          <Input
            name="review"
            label="Review : "
            placeholder="5"
            error={errors["review"]}
            onChange={onHanndleChangeValidate}
          />
          {errors["image"] ? (
            <div className="text-red-500">{errors["image"]}</div>
          ) : (
            <div></div>
          )}
          <input type="file" name="image" />
          <br />
          <button
            className="flex float-right bg-purple-500 hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-purple-600 text-white py-2 px-5 cursor-pointer"
            type="submit"
            disabled={loading}
          >
            {loading && (
              <svg
                className="animate-spin -ml-1 mr-3 h-5 w-5 text-white"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <circle
                  className="opacity-25"
                  cx="12"
                  cy="12"
                  r="10"
                  stroke="currentColor"
                  strokeWidth="4"
                ></circle>
                <path
                  className="opacity-75"
                  fill="currentColor"
                  d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                ></path>
              </svg>
            )}

            <span>POST</span>
          </button>
        </form>
        <br />
      </div>
      <br />
      <br />
      <br />
    </>
  );
}
