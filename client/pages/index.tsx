import Head from "next/head";

import ArticleList from "../components/article/ArticleList";

import headerStyles from "../styles/newStyles/Header.module.css";

import Image from "next/image";
import Link from "next/link";
import Card from "../components/Card";
import { useEffect, useState } from "react";
import axios from "axios";

export default function Home() {
  const [data, setData] = useState([]);
  useEffect(() => {
    axios
      .get(`${process.env.NEXT_PUBLIC_SERVER}/product`)
      .then((res) => setData(res.data["results"]));
  });
  return (
    <>
      <div className="relative w-full h-full">
        <Image
          className="bg-purple-300 h-full"
          width={2000}
          height={700}
          layout="intrinsic"
          quality={100}
          objectFit="cover"
          src={
            "https://images.pexels.com/photos/3183132/pexels-photo-3183132.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
          }
        />

        <div className="absolute top-4 left-4">
          <h2 className="text-7xl text-white">
            WELCOME TO <br />W<span className="text-purple-600">-</span>REVIEWS
          </h2>
          <br />
          <Link href="/login">
            <a className="text-white bg-purple-600 px-10 py-3"> GET START </a>
          </Link>
        </div>
      </div>
      <div className="grid sm:grid-cols-3 grid-cols-1 gap-2 mt-1">
        {data.map((data,i)=>{
          return <Card key={i} id={data["_id"]} image={data["image"]} name={data["name"]} />
        })}
      </div>
      <br /><br /><br />
    </>
  );
}
