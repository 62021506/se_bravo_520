import Head from "next/head";

import ArticleList from "../components/article/ArticleList";

import headerStyles from "../../styles/newStyles/Header.module.css";
import { useState } from "react";

import axios from "axios";
import Link from "next/link";
import Input from "../components/Input";
import { useRouter } from "next/router";

export default function Home(props: any) {
  const [errors, setErrors]: any = useState({});
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  const inputValidate = (body: any) => {
    let errorTag: any = {};
    let isValid = true;
    if (!body.username) {
      isValid = false;
      errorTag["username"] = "Username is required";
    }

    if (!body.password) {
      isValid = false;
      errorTag["password"] = "Password is required";
    }
    setErrors(errorTag);
    setLoading(false);
    return isValid;
  };

  const loginSystem = (e: any) => {
    let errorTag: any = {};
    e.preventDefault();

    const body = {
      username: e.target.username.value,
      password: e.target.password.value,
      rememberMe: e.target.checkbox.checked,
    };
    if (!inputValidate(body)) return;

    setLoading(true);
    axios
      .post(`${process.env.NEXT_PUBLIC_SERVER}/auth/login`, body)
      .then((res: any) => {
        if (!res.data.jwt) {
          errorTag["username"] = "Username or Password is invalid";
          errorTag["password"] = "Username or Password is invalid";
          setErrors(errorTag);
          setLoading(false);
          return;
        }
        setLoading(false);
        localStorage.setItem("jwt", res.data.jwt);
        router.push("/");
      });
  };

  const onHanndleChangeValidate = (e: any) => {
    const { name } = e.target;
    setErrors({
      ...errors,
      [name]: "",
    });
  };

  return (
    <>
      <div className="bg-white sm:p-11 p-5 shadow-md m-auto w-4/6">
        <h1 className="text-3xl font-romeo2">Sign In</h1>
        <span className="text-lg mt-1 block">
          Welcome back to the community
        </span>
        <br />
        <form onSubmit={loginSystem}>
          <Input
            name="username"
            label="Username"
            placeholder="romeo"
            error={errors["username"]}
            onChange={onHanndleChangeValidate}
          />
          <Input
            name="password"
            label="Password"
            placeholder="password"
            error={errors["password"]}
            onChange={onHanndleChangeValidate}
            password
          />
          <div className="mb-2 block">
            <input
              name="checkbox"
              className="form-checkbox border-purple-700 text-purple-500 focus:outline-none focus:ring-2 focus:ring-purple-600"
              id="checkbox"
              type="checkbox"
              onChange={onHanndleChangeValidate}
            />
            <label htmlFor="checkbox" className="ml-2 select-none">
              Remember me
              {errors["checkbox"] && (
                <div className="text-sm text-red-500">{errors["checkbox"]}</div>
              )}
            </label>
          </div>
          <button
            className="flex bg-purple-500 hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-purple-600 text-white py-2 px-5 cursor-pointer"
            type="submit"
            disabled={loading}
          >
            {loading && (
              <svg
                className="animate-spin -ml-1 mr-3 h-5 w-5 text-white"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <circle
                  className="opacity-25"
                  cx="12"
                  cy="12"
                  r="10"
                  stroke="currentColor"
                  strokeWidth="4"
                ></circle>
                <path
                  className="opacity-75"
                  fill="currentColor"
                  d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                ></path>
              </svg>
            )}

            <span>Sign in</span>
          </button>
        </form>
        <br />
        <div className="flex justify-between">
          <Link href="/forgotpwd">
            <a className="text-purple-600">Forgot password?</a>
          </Link>
          <Link href="/register">
            <a className="text-purple-600">Create account</a>
          </Link>
        </div>
      </div>
    </>
  );
}
