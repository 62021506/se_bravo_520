import Head from "next/head";

import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";

export default function Home(props: any) {
  const router = useRouter();

  const [data, setData]: any = useState({});
  useEffect(() => {
    axios
      .get(`${process.env.NEXT_PUBLIC_SERVER}/product/${router.query.id}`)
      .then((res) => setData(res.data));
  });

  return (
    <>
      <div className="flex">
        <div className="flex-shrink w-4/12">
          <div className=" w-full">
            <Image
              className="bg-purple-300 h-full"
              width={500}
              height={300}
              layout="intrinsic"
              quality={100}
              objectFit="cover"
              src={`${
                data.image ||
                "https://images.pexels.com/photos/3183132/pexels-photo-3183132.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
              }`}
            />
          </div>
        </div>
        <div className="flex-grow w-8/12">
          <div className="w-full pl-3">
            <h1 className="text-5xl">
              {data?.name?.toString()?.toUpperCase()}
            </h1>
            <div className="py-2">
              TYPE : {data?.type?.toString()?.toUpperCase()}
            </div>
            <div className="border-2 p-3">{data?.detail?.toString()}</div>
          </div>
        </div>
      </div>
    </>
  );
}
