import Link from 'next/link'

const article = ({article}) => {
    return (
        <>
            <h1>This is an article wewewe {article.id}</h1>
            <h3>{article.title}</h3>
            <p>{article.body}</p>
            <br></br>
            <Link href='/home'>Go Back </Link>
        </>
    ) 
}

export const getStaticProps = async(context) => {
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${context.params.id}`)

    const article = await res.json()

    return {
        props: {
            article,
        },
    }
}

export const getStaticPaths = async () => {
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts`) 

    const article = await res.json()

    const ids = article.map(article => article.id)

    const paths = ids.map((id) => ({params: {id: id.toString() } }))

    return {
        paths,
        fallback: false,
    }
}

export default article