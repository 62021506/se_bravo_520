import Head from 'next/head'

import headerStyles from '../../styles/newStyles/Header.module.css'

// import searchStyle from '../../styles/testAPI/searchStyle.css'

export default function Home({ articles }) {
    return (
        <div>

            <Head>
                <title>search</title>
                <meta name='keyword' content='web development, programming' />
            </Head>

            <body>
                
                <h1 className={headerStyles.title}>
                    <span>WebDev</span> search
                </h1>

                <p className={headerStyles.description}>find something</p>

                <br/>

                <form className="search">
                    <input name="query" type="search"></input>
                    <button>search</button>
                </form>
                 
            </body>

            <style> {`
                .search {
                    margin-top: 2em;
                }
                .search input {
                    padding: .3em .6em;
                    margin-right: 1em;
                }
                @media (max-width: 600px) {
                    .search input {
                    margin-right: 0;
                    margin-bottom: .5em;
                    }
                }
                .search input,
                .search button {
                    font-size: 1.4em;
                }
            `}</style>

        </div>
    )
    
}

