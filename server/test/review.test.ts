import app from "../src/app";
import request from "supertest";
import mongoose from "mongoose";

let jwt = "";

describe("Sample Test Review", () => {
  it("get jwt", async () => {
    const res = await request(app).post("/auth/login").send({
      username: "romeo55",
      password: "test",
      rememberMe: true,
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body.status).toEqual(true);
    jwt = res.body.jwt;
  });
  it("should test that true", async () => {
    const res = await request(app)
      .post("/product/add")
      .set({
        authorization: `Bearer ${jwt}`,
      })
      .send({
        image:
          "https://firebasestorage.googleapis.com/v0/b/noname-71987.appspot.com/o/1.png?alt=media&token=2435ca8d-cb27-4a9f-8b8f-38a3fdd8f9e1",
        type: "type2",
        name: "name2",
        detail: "detail2",
        review: "review2",
      });
    expect(res.statusCode).toEqual(200);
    expect(res.body.status).toEqual(true);
  });
});

afterAll(() => mongoose.disconnect());
