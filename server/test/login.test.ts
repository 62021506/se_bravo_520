import app from "../src/app";
import request from "supertest";
import mongoose from "mongoose";

describe("Sample Test Login", () => {
  it("should test that true", async () => {
    const res = await request(app).post("/auth/login").send({
      username: "romeo55",
      password: "test",
      rememberMe: true,
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body.status).toEqual(true);
  });
  it("should test that false", async () => {
    const res = await request(app).post("/auth/login").send({
      username: "s",
      password: "s",
      rememberMe: true,
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body.status).toEqual(false);
  });
});

afterAll(() => mongoose.disconnect());
