import app from "../src/app";
import request from "supertest";
import mongoose from "mongoose";

describe("Sample Test Register", () => {
  it("should test that true", async () => {
    const res = await request(app).post("/auth/register").send({
      username: "romeo55666",
      password: "test",
      conPassword: "test",
      name: "romeo",
      email: "romeo@gmail.com",
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body.status).toEqual(true);
  });
  it("should test that Username already exists", async () => {
    const res = await request(app).post("/auth/register").send({
      username: "romeo55",
      password: "test",
      conPassword: "test",
      name: "romeo",
      email: "romeo@gmail.com",
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body.status).toEqual('Username already exists');
  });
});

afterAll(() => mongoose.disconnect());
