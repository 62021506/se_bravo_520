import express, { Application, Request, Response } from "express";
import {
  Login,
  Register,
  Delete,
  Edit,
  Product,
  AddReviews,
} from "./controller";
const app: Application = express();
import cors from "cors";
import db from "./db/mongo";

app.use(cors());
db;
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(express.json());

app.get(
  "/",
  async (req: Request, res: Response) => {
    res.json({ status: "welcome" });
  }
);
app.use("/auth", Login);
app.use("/auth", Register);
app.use("/auth", Delete);
app.use("/auth", Edit);

app.use("/product", Product);
app.use("/reviews", AddReviews);

export default app;
