import mongoose from "mongoose";

const uri = process.env.MONGO || "mongodb://mongo:27017/bravo";

mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("database ok"));

export default db;
