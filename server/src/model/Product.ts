import mongoose from "mongoose";

const ProductSchema = new mongoose.Schema({
  image: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  detail: {
    type: String,
    required: true,
  },
});

const product = mongoose.model("Product", ProductSchema);

export default product;
