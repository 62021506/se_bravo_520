import app from "./app"

app.listen(process.env.PORT||3001, () => {
  console.log(`RUNING PORT ${process.env.PORT||3001}`);
});

export default app
