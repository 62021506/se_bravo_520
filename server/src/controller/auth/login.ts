import express, { Router, Request, Response, NextFunction } from "express";
import { User } from "../../model";
import {deCode,generateJWT} from "../../components"

const router: Router = express.Router();

router.get("/", async (req: Request, res: Response) => {
  const data = await User.find({}).lean();
  console.log(data);
  res.send(data);
});

const checkValidate = (req: Request, res: Response, next: NextFunction) => {
  if (!req.body.username) return res.json({ status: "username require" });
  if (!req.body.password) return res.json({ status: "password require" });
  if (req.body.rememberMe != true && req.body.rememberMe != false) return res.json({ status: "rememberMe[boolean] require" });
  next();
};

router.post("/login", checkValidate, async (req: Request, res: Response) => {
  const { username, password,rememberMe } = req.body;
  let status = false;
  let jwt = "";
  const dataLogin = await User.findOne({ username }).lean();
  if (dataLogin && await deCode(password,dataLogin.password)) {
    status = true;
    jwt = await generateJWT(dataLogin,rememberMe);
  }
  res.json({ status,jwt });
});

export default router;
