import express, { Router, Request, Response, NextFunction } from "express";
import { User } from "../../model";
import { hash } from "../../components";

const router: Router = express.Router();

const checkValidate = (req: Request, res: Response, next: NextFunction) => {
  if (!req.body.username) return res.json({ status: "username require" });
  if (!req.body.password) return res.json({ status: "password require" });
  if (!req.body.name) return res.json({ status: "name require" });
  if (!req.body.email) return res.json({ status: "email require" });
  next();
};

router.post("/register", checkValidate, async (req: Request, res: Response) => {
  const { username, password, name, email } = req.body;
  const checkUsername = await User.findOne({ username }).lean();
  if (checkUsername) {
    res.json({ status: "Username already exists" });
    return 0;
  }
  let enPassword: string = await hash(password);

  await User.create({ username, password: enPassword, name, email });
  res.json({ status: true });
});

export default router;
