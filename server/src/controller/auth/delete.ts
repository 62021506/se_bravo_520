import express, { Router, Request, Response, NextFunction } from "express";
import { User } from "../../model";
import mongoose from "mongoose";
import { checkTokenViaId } from "../../components"

const router: Router = express.Router();

router.delete("/delete/:id", checkTokenViaId, async (req: Request, res: Response) => {
  const id = mongoose.Types.ObjectId(req.params.id);

  const data = await User.remove({ _id: id });
  console.log(data);
  res.json({ status: `delete ${id} success!!!` });
});

export default router;
