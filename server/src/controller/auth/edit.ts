import express, { Router, Request, Response, NextFunction } from "express";
import { User } from "../../model";
import mongoose from "mongoose";
import { checkTokenViaId } from "../../components"

const router: Router = express.Router();

const checkValidate = (req: Request, res: Response, next: NextFunction) => {
  if (!req.body.password) return res.json({ status: "password require" });
  if (!req.body.conPassword)
    return res.json({ status: "conPassword require" });
  next();
};

router.post("/edit/:id", [checkValidate, checkTokenViaId], async (req: Request, res: Response) => {
  const id = mongoose.Types.ObjectId(req.params.id);
  const { password, conPassword } = req.body;
  if (password !== conPassword) {
    return res.json({ status: "Password not match" });
  }
  const edited = await User.findByIdAndUpdate(
    { _id: id },
    { password: password }
  );
  res.send(edited);
});

export default router;
