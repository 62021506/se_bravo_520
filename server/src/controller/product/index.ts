import express, { Application, Request, Response } from "express";
import Add from "./add";
import Views from "./views";
const app: Application = express();

app.use(Add);
app.use(Views);

export default app;
