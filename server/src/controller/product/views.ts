import express, { Router, Request, Response, NextFunction } from "express";
import { Product } from "../../model";
import mongoose from "mongoose";

const router: Router = express.Router();

router.get("/", async (req: Request, res: Response) => {
  const data = await Product.find({});
  res.json({ results: data });
});

router.get("/:id", async (req: Request, res: Response) => {
  const { id = "" } = req.params;
  if (!mongoose.Types.ObjectId.isValid(id)) return res.json({ status: false });
  const data = await Product.findOne({ _id: id }).lean();
  res.json(data);
});

export default router;
