import Login from "./auth/login";
import Register from "./auth/register";
import Delete from "./auth/delete";
import Edit from "./auth/edit";

import Product from "./product";
import AddReviews from "./reviews/add";

export { Login, Register, Delete, Edit, Product, AddReviews };
