import express, { Router, Request, Response, NextFunction } from "express";
import { checkTokenJWT } from "../../components/jwt";
import { Product } from "../../model";

const router: Router = express.Router();

const checkValidate = (req: Request, res: Response, next: NextFunction) => {
  if (!req.body.image) return res.json({ status: "image require" });
  if (!req.body.type) return res.json({ status: "type require" });
  if (!req.body.name) return res.json({ status: "name require" });
  if (!req.body.detail) return res.json({ status: "detail require" });
  if (!req.body.product) return res.json({ status: "product(id) require" });
  next();
};

router.post(
  "/add",
  [checkTokenJWT, checkValidate],
  async (req: Request, res: Response) => {
    res.json({ status: true });
  }
);

export default router;
