import jwt, { decode } from "jsonwebtoken";
import express, { Router, Request, Response, NextFunction } from "express";

const SECRET = process.env.SECRET || "shadow";

const generateJWT = async (data: any, rememberMe: boolean) => {
  return await jwt.sign({ userId: data._id, name: data.username }, SECRET, {
    expiresIn: rememberMe ? "365d" : "7d",
  });
};

const checkJWT = (token: string): any => {
  if (token) {
    try {
      return jwt.verify(token.split(" ")[1], SECRET);
    } catch (error) {
      return null;
    }
  }
  return null;
};

const checkTokenJWT = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.headers.authorization)
    return res.json({ status: "header authorization require" });
  if (checkJWT(req.headers ? req.headers.authorization : "") == null)
    return res.json({ status: "header authorization not pass" });
  next();
};

// check token via id
const checkTokenViaId = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.headers.authorization)
    return res.json({ status: "header authorization require" });
  if (checkJWT(req.headers ? req.headers.authorization : "") == null)
    return res.json({ status: "header authorization not pass" });
  if (
    checkJWT(req.headers ? req.headers.authorization : "") != null &&
    checkJWT(req.headers ? req.headers.authorization : "").userId !=
      req.params.id
  )
    return res.json({ status: "not pass" });
  next();
};

export { generateJWT, checkJWT, SECRET, checkTokenViaId, checkTokenJWT };
