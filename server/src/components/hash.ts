import bcrypt from 'bcrypt';

const saltRounds = 10;

const hash = async (password: string) => {
    return await bcrypt.hash(password, saltRounds);
}

const deCode = async (password: string, hash: string) => {
    return await bcrypt.compare(password, hash);
}

export { hash, deCode }